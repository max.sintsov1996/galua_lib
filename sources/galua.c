int LFSR_Galois (void)
{
    // for polynomial 0x80000057, reversed 0xea000001

    static unsigned char S = 0x1;
    unsigned char reversed_polinom = 0b1101;

    if (S & 0x1) {
        S = ((S ^ reversed_polinom) >> 1) | 0b1000;
        return 1;}
    else {
        S >>= 1;
        return 0;}
}
