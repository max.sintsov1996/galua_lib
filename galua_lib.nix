with (import <nixpkgs> {});
let
  inherit stdenv fetchurl gzip gnutar coreutils;
 
galua_lib = stdenv.mkDerivation rec{
    version = "7f8501d06056fc07b0339b9fc4cb22baf6e8870e";
    name = "galua_lib";

    src = fetchurl {
      url = "https://gitlab.com/max.sintsov1996/galua_lib/-/archive/${version}/galua_lib-${version}.tar.gz";
      sha256 = "062zncb2ry52cq13xljkmq3idk215vjbnq5m3pkqpj23gcgbxfwh";
    };

  unpackPhase = ''
    mkdir $out
    mkdir $out/tmp
    tar -C $out/tmp -xvf $src 
  '';

  installPhase = ''
    gcc -c -Wall -Werror -fpic -o $out/tmp/galua.o $out/tmp/$name-$version/sources/galua.c
    gcc -shared -o $out/galua_lib.so $out/tmp/galua.o
    rm -r $out/tmp
  '';

  system = builtins.currentSystem;
};

in galua_lib
